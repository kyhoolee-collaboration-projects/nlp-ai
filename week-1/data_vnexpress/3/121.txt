Cháy nhà kho 2.000m2 sau chợ Vinh
Cháy nhà kho ở phía sau chợ Vinh.
Hơn 13h ngày 9/12, kho hàng nằm trên đường Bến Đền, phường Vinh Tân (TP Vinh, Nghệ An) bốc cháy khi trời đang mưa nhỏ. Trong chốc lát, cột khói cuộn cao cả chục mét, mùi nhựa khét lẹt. Địa điểm cháy nằm phía sau và cách chợ Vinh 100 m. Một số người hô hoán nhau dùng bình cứu hỏa mini dập lửa, nhưng bất thành.
Công an Nghệ An đã huy động 15 xe cứu hỏa, 500 cán bộ chiến sĩ tiếp cận hiện trường từ nhiều hướng, giúp dân di tản hàng hóa ở hai kho bên cạnh ra ngoài, tránh cháy lan.
"Chúng tôi phát hiện cháy thì lập tức gọi điện và thông báo lên Facebook cá nhân để cầu cứu người thân, bạn bè ở gần chạy tới dập lửa, cứu hàng hóa ra ngoài", anh Thanh, một người kinh doanh gần đám cháy, kể.
Lửa trong nhà kho bốc lên ngùn ngụt. Ảnh: CTV
Hơn một giờ sau, nhà kho cấp bốn, lợp tôn rộng khoảng 2.000 m2 vẫn bị lửa bao trùm. Hàng chục vòi rồng luồn lách vào các ngõ, trên mái nhà của kho bên cạnh để phun nước. Nhiều vách tường được đục thủng để luồn vòi nước vào.
Tới chỉ đạo chữa cháy, đại tá Nguyễn Hữu Cầu, Giám đốc Công an tỉnh Nghệ An nói: "Kho hàng chứa rất nhiều đồ nhựa, khi bị lửa bùng cháy thì khói bốc lên nhiều. Hai bên kho hàng là hai dãy nhà kho khác, tất cả đều lợp mái tôn là những trở ngại khi dập lửa".
Đến hơn 16h, đám cháy được cơ bản khống chế. Công an Nghệ An vẫn điều thêm hai xe cứu hỏa của sân bay Vinh và khu công nghiệp tới để dập tắt đám cháy hoàn toàn.
Người dân khẩn trương di dời tài sản. Ảnh: Nguyễn Hải
Tại hiện trường, nhà kho nơi phát ra đám cháy có hàng chục tấn hàng hóa gồm đồ nhựa, bát đĩa sứ, chiếu nhựa bị lửa thiêu rụi, xém đen. Dọc tuyến đường Bến Đền và Thái Phiên, hàng trăm tấn hàng hóa chất đống, phải dùng bạt che.
Hỏa hoạn khiến một người trong lúc tham gia dập lửa bị ngã từ trên cao xuống đất, gây chấn thương nhẹ. Công an đang thống kê thiệt hại, khám hiện trường để làm rõ nguyên nhân cháy.
Khói bốc cao hàng chục mét, mùi khét lẹt. Ảnh: Nguyễn Hải