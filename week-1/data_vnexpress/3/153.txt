Nước rút chậm, nhà dân Tam Kỳ ngập gần 1,5 m
Rạng sáng 11/12, Quảng Nam bắt đầu tạnh mưa, nước rút chậm nên nhiều khu phố ở TP Tam Kỳ ngập cục bộ, một số khu bị chia cắt.
Đường Trần Quý Cáp ngập nửa mét, nhiều tuyến phố khác biến thành sông, giao thông tê liệt. Đợt này, các huyện miền núi mưa ít, chủ yếu là vùng đồng bằng, gồm TP Tam Kỳ, hai huyện Thăng Bình, Phú Ninh.
Quốc lộ 1A đoạn qua Tam Kỳ ngập nửa mét, cảnh sát giao thông chốt chặn, hướng dẫn xe ôtô đi lên cao tốc Đà Nẵng - Quảng Ngãi.
Trường Tiểu học Nguyễn Văn Trỗi, phường Tân Thạnh bị nước ngập trước cổng khoảng 30 cm. Hôm nay, học sinh ở Tam Kỳ được nghỉ học.
Khối phố Trường Đồng, phường Tân Thạnh nằm ven sông Bàn Thạch ngập sâu nhất. Người dân di chuyển bằng ghe thuyền trên đường phố.
Tại khu phố này, nhiều căn nhà vẫn ngập hơn một mét, người dân phải sơ tán lên chỗ cao ráo.
Tầng dưới bị ngập nước, gia đình bà Kim ở khối phố Trường Đồng sinh hoạt ở tầng trên. “Trước khi lũ về, tôi di chuyển tài sản lên tầng hai, những ngày qua lấy nước mưa ăn uống nên không bị đói”, bà nói và cho hay trận lũ này "nước lên nhanh chưa từng thấy".
Nhà ông Bùi Viết Tươi, khối phố Trường Đồng, đang ngập hơn nửa mét nên ông kê ghế ngồi chờ nước rút.
Theo người dân Trường Đồng, nước lũ rút chậm, nếu trời không mưa thì hai ngày nữa mới hết ngập.
Tài sản của ông Trần Văn Thanh trôi trong nước. “Mặc dù tôi đã kê lên cao nhưng nước về quá nhanh và ngập sâu nên tài sản thiệt hại gần hết”, ông nói và cho hay nước ngập tương đương trận lụt năm 1999.
Hai ngày qua nhà bị ngập, anh Trần Văn Tiến đi sơ tán. Sáng nay nước rút được 30 cm, anh chèo ghe để về thăm nhà. “Lúa gạo, tivi, tủ lạnh… bị ngập hết rồi”, anh nói và cho hay chỉ chuyển được ít lợn lên cao tránh lũ. Hai ngày qua lợn bị bỏ đói, giờ anh mang xoong nồi lên nấu cám cho chúng ăn.
Trời nắng, người dân phường Tân Thạnh đưa lúa bị ngập ra đường phơi. Trước đó, do ảnh hưởng của gió mùa đông bắc và nhiễu động gió đông, Quảng Nam cũng như các tỉnh miền Trung có mưa to. Hai trạm Câu Lâu và Bình Phú mưa ba ngày qua lên tới 800 mm, riêng trạm Thăng Bình tới 930 mm. Toàn tỉnh có hai người chết, một người mất tích, 17.320 nhà bị ngập.