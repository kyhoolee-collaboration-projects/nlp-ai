Sáng 10/12, dù nước rút sau khi dâng lên đêm qua nhưng tỉnh Quảng Ngãi tiếp tục có mưa to ở nhiều nơi.
Tại huyện Bình Sơn, hàng trăm nhà dân thôn Long Yên, xã Bình Long vẫn bị ngập sâu, chia cắt hoàn toàn với bên ngoài. Một người đàn ông đã mất tích trên sông Trà Bồng khi nước sông đang dâng cao.
Trong khi đó, dù nước rút dần nhưng đến trưa nay, trường Tiểu học Phổ Văn, huyện Đức Phổ vẫn ngập trong nước. Sở Giáo dục Quảng Ngãi đã cho toàn bộ học sinh nghỉ học từ hôm nay.
Nhà dân ở xã Phổ Văn bị nước bốn bề vây quanh sau trận mưa đêm qua.
Theo cáo cáo của huyện Đức Phổ, đến sáng nay có hơn 1.000 hộ dân bị ngập nhà; 170 ha hoa màu bị ngập úng; 8.600 con gia cầm bị chết trong lũ; nhiều tuyến đường, kênh mương bị sạt lở. Nước sông Trà Câu ở mức báo động 3 và đang xuống chậm.
Đứng trước sân nhà mênh mông nước, ông Nguyễn Ngọc Sơn đưa bàn tay trước ngực chỉ độ sâu nước ngập. Ông cho biết, nước lên nhanh nhất lúc 0h đến 2h sáng nay, khiến nhà ông ngập 1-1,5 m.
Ông Phạm Quang ở xã Phổ Văn cho biết, gia đình ông đã kê nhiều vật dụng lên cao nhưng vẫn bị ướt. "Xót nhất là mấy bao lúa làm vụ trước, đã kê lên ghế nhưng nước năm nay cao hơn năm trước", ông nói và cho biết, đêm qua, ông phải kê giường lên trên bàn phòng khách để con ngủ nhưng vẫn bị ướt lưng.
Ông Quang múc nước dội bùn đọng lại khi nước lũ dần rút.
Ở xã Hành Dũng, huyện Nghĩa Hành, nơi được xem là vùng rốn lũ, nước bắt đầu rút sau hai ngày mưa lớn, để lại bùn đất. UBND xã Hành Dũng đã huy động toàn bộ nhân viên dọn bùn.
Trường Mầm non Hành Dũng bị ngập một mét, nước lũ tràn vào các phòng học trong hai ngày qua. Sáng nay nước rút, nhiều chiến sĩ bộ đội đến giúp trường dọn bùn.
Đường nối hai xã Ba Dinh và Ba Giang, huyện Ba Tơ bị sạt lở 300 m.
Sau hai ngày mưa lớn, huyện miền núi Ba Tơ có 30 điểm sạt lở. Tại các huyện Nghĩa Hành, Đức Phổ, Bình Sơn ước tính có hàng nghìn nhà dân bị ngập sâu, cô lập.
Theo Đài Khí tượng thủy văn Quảng Ngãi, ngày 10/12, lượng mưa phổ biến trong tỉnh 40 - 80 mm. Riêng các huyện Minh Long, Ba Tơ, Sơn Hà, Trà Bồng có mưa rất to với lượng 100 - 150 mm. Mưa lớn gây nguy cơ lũ quét và sạt lở đất trên các huyện miền núi, ngập lụt các vùng trũng thấp, ven sông suối.