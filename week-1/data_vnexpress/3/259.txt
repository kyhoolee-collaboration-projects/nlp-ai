Đóng cọc sát ngăn bờ kè sạt lở ở Thành cổ Quảng Trị
Đóng 30 cọc sắt ngăn bờ kè Thành cổ Quảng Trị tiếp tục sạt lở.
Chiều 11 và ngày 12/12, Ban quản lý di tích tỉnh Quảng Trị cho đóng cọc sắt tại điểm kè xung yếu, có nguy cơ đổ sập ở bờ hồ quanh di tích quốc gia đặc biệt Thành cổ Quảng Trị (thị xã Quảng Trị).
Nhà chức trách cho đóng 30 cọc sắt chữ I dài sáu mét xuống sát bờ kè nhằm khắc phục tạm thời. Vị trí đóng cọc sát hai đầu của điểm sạt lở và kéo dài ra ở các điểm xung yếu. Tổng chiều dài đoạn kè được đóng cọc khoảng 50 m.
Ban quản lý di tích tỉnh Quảng Trị cho đóng cọc sắt sát bờ kè hồ quanh Thành cổ Quảng Trị ngăn sạt lở. Ảnh: Hoàng Táo
Ông Nguyễn Quang Chức, Giám đốc Ban quản lý di tích tỉnh cho hay, sau khi đóng cọc sắt, Ban sẽ cho nạo vét đất cát, gạch bị sạt xuống bờ hồ, căng dây để đảm bảo an toàn và mỹ quan. Về lâu dài, đơn vị sẽ đầu tư xây lại hệ thống bờ kè nhằm sử dụng lâu dài và an toàn.
Sau trận mưa lớn ngày 8/12, bờ kè quanh hồ xuất hiện hai điểm sạt lở, tổng chiều dài 60 m. Hiện, tại vị trí sạt ở phía đường Hai Bà Trưng xuất hiện thêm nhiều điểm bờ kè nứt vỡ, nguy cơ đổ sập.
Hệ thống bờ kè này được xây từ 14 năm trước. Mùa mưa các năm trước, nhiều điểm kè cũng bị sạt lở.