Bộ Quốc phòng vừa ban hành Thông tư Quy định đảm bảo xe phục vụ lễ Quốc tang và lễ tang cấp Nhà nước.
Thông tư áp dụng đối với các cơ quan, đơn vị thuộc Bộ Quốc phòng làm nhiệm vụ nghi lễ Quốc tang, lễ tang cấp Nhà nước.
Đoàn xe phục vụ Quốc tang cố Tổng bí thư Đỗ Mười tháng 10/2018. Ảnh: Tất Định.
Theo đó, lễ Quốc tang gồm 15 xe ôtô và một xe chở linh cữu (linh xa). Trong đó, đoàn xe chỉ huy có: xe chở quốc kỳ, ảnh, gối huân huy chương; xe chở quân kỳ; xe chỉ huy.
Sáu xe vận tải chở đội hình danh dự. Ba xe vận tải ba cầu (xe kéo linh xa, xe chở hoa, xe dự phòng). Ngoài ra còn có hai xe thông tin, một xe cứu thương. Linh xa phục vụ quốc tang phía cuối là khẩu lựu pháo 122mm.
Xe phục vụ lễ tang cấp Nhà nước gồm 11 xe và một linh xa. Trong đó có hai xe chỉ huy (xe chở quân kỳ, ảnh, gối huân huy chương; xe chỉ huy). Ba xe vận tải chở đội hình danh dự. Ba xe vận tải ba cầu (xe kéo linh xa, xe chở hoa, xe dự phòng). Hai xe thông tin, một xe cứu thương.
Linh xa phục vụ lễ tang cấp Nhà nước phía cuối có bệ với hàng chữ "Tổ quốc ghi công".
Bộ Quốc phòng yêu cầu chất lượng đoàn xe phục vụ lễ tang phải luôn có kỹ thuật tốt, đồng bộ, hình thức đẹp, trang trọng, phù hợp với nhiệm vụ và truyền thống dân tộc, thuận lợi cho công tác đảm bảo kỹ thuật.
Xe ôtô phục vụ lễ tang sử dụng không quá 15 năm từ khi xuất xưởng; linh xa sử dụng không quá 10 năm phải được sửa chữa.
Để đảm bảo kỹ thuật, hàng tuần các xe phục vụ lễ tang phải được kiểm tra kỹ thuật, mỗi xe nổ máy tại chỗ 15 phút, chạy thử trong đơn vị 1-2 km; hàng tháng chạy thử ngoài đơn vị 15-20 km, hàng quý chạy thử 25-30km.
Thông tư có hiệu lực thi hành từ 18/1/2019, thay thế Quyết định số 9 của Bộ trưởng Quốc phòng năm 2008 về Quy chế đảm bảo xe phục vụ lễ tang.