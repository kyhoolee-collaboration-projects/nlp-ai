Hẻm 55 Nguyễn Thông (phường 9, quận 3) những ngày này chộn rộn vì người dân thường bàn chuyện hiến đất cho chính quyền mở hẻm. Trong đó, ông Lê Văn Nguơn (86 tuổi) đã nhường 12 m2 đất của gia đình, có giá thị trường khoảng 1,2 tỷ đồng.
"Số tiền đó tôi có thể mua một căn nhà vùng ven nhưng hiểu được ý nghĩa của việc hiến đất nên tôi và bà con ở đây rất sẵn sàng", ông Nguơn cười, móm mém nói.
Ông cụ kể, trước đây con hẻm khá nhếch nhác, dài chưa đến 200 m, rộng gần 3 m. Các hộ dân đều sống trong cảnh chật chội nên khi xảy ra hỏa hoạn, có người bệnh cần cấp cứu, các xe chuyên dụng rất khó tiếp cận. Hay khi nhà nào có cưới hỏi, ma chay đều rất bí bách. Thấy được những hạn chế trên, chính quyền địa phương vận động người dân hiến đất mở hẻm.
Ông Lê Văn Nguơn nói về việc cùng hàng xóm hiến đất. Ảnh: Dương Trang.
Phong trào này cũng được phường 9 áp dụng với hẻm 85 Rạch Bùng Binh, dài 200 m và trước đây rộng chừng hơn một mét. An toàn cháy nổ ở khu vực này rất thấp, đã có một số vụ hỏa hoạn xảy ra và việc chữa cháy là rất khó khăn.
Sống tại đây nhiều năm, ông Nguyễn Xuân Lộc (60 tuổi) giọng phấn khởi: "Chính quyền hỗ trợ mặt hạ tầng kỹ thuật, người dân hy sinh đất. Đất của chúng tôi hơn 3 cây vàng một mét đấy. Cân nhắc mãi, động viên nhau cùng thực hiện nên giờ con hẻm rộng hơn 4 m, rất thoáng mát".
Tương tự, mỗi hộ dân tại hẻm 19 Cô Bắc (phường 1, quận Phú Nhuận) cũng tình nguyện hiến hơn một mét chiều dài nhà (khoảng 3-4 m2), có giá 90-110 triệu đồng một mét vuông.
Ông Lê Doãn Minh (71 tuổi) cho biết, đối diện nhà ông trước đây là ruộng rau muống. Mỗi chiều đi làm về cha ông lại chở một xe tải đất về lấp ao, làm thành con hẻm rộng 6 m cho người dân đi lại. Về sau, nhiều người mở rộng khuôn viên nhà, lấn dần ra hẻm.
Khi chính quyền địa phương bàn chuyện mở rộng hẻm, đa số hộ dân ủng hộ bởi ngoài việc góp phần chỉnh trang đô thị sạch đẹp, việc này khiến giao thông thuận tiện, xử lý cứu hỏa, cứu nạn kịp thời. Ngoài ra, về lâu dài giá trị căn nhà của người dân được tăng lên.
"Nếu mở hẻm rộng thêm nữa, thông thoáng, có lợi cho người dân tôi sẵn sàng hiến thêm", ông Minh chia sẻ.
Hẻm 19 Cô Bắc đang trong quá trình mở rộng. Ảnh: Dương Trang.
Trao đổi với VnExpress, Phó chủ tịch phường 9 (quận 3) Nguyễn Ngọc Bình cho biết, việc vận động người dân không dễ, vì đất ở quận hiện có giá cả trăm triệu đồng mỗi mét vuông. Hẻm 55 có 42 hộ nhưng chỉ có 30 nhà đồng ý hiến đất, dù chính quyền và bà con đã nỗ lực rất nhiều.
"Ngoài tiếp tục vận động, phường thực hiện mở hẻm theo hình thức 'cuốn chiếu' để người dân thấy được lợi ích mà đồng thuận", ông Bình nói.
Dương Trang