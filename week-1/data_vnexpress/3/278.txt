Bên trong cơ sở bị cháy khiến hai người bỏng nặng. Ảnh: Quế Biên.
Sáng 9/12, nhóm người đang sang chiết xăng dầu tại cơ sở trên đường Hoàng Hữu Nam (phường Long Thạnh Mỹ, quận 9, TP HCM) thì bất ngờ lửa bùng lên. Lửa lan nhanh, cháy ngùn ngụt khiến mọi người bỏ chạy tán loạn.
Ông Lê Thành Mâu (48 tuổi) và ông Lê Văn Ảnh (65 tuổi) không kịp chạy đã bị lửa bén vào người. Cả hai được dập lửa, chuyển lên Bệnh viện Chợ Rẫy cấp cứu trong tình trạng nguy kịch.
Hỏa hoạn được cảnh sát PCCC dập tắt ngay sau đó, khi đã thiêu rụi một số vật dụng bên trong.
Theo người dân quanh khu vực, cơ sở sang chiết xăng dầu này hoạt động theo kiểu "kín cổng cao tường". Mỗi ngày, xe ra vào khá thường xuyên, bên trong có nhiều thùng phuy cỡ lớn.
Sơn Hoà