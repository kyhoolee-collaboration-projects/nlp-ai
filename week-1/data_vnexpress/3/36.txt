Xưởng đóng tàu xảy ra vụ nổ. Ảnh: Sơn Hoà.
Sáng 14/12, người dân sống gần xưởng đóng tàu biển An Phú trên đường Đào Trí, phường Phú Thuận (quận 7) nghe tiếng nổ lớn. Bên trong xưởng nhốn nháo, một số người nằm la liệt.
Cảnh sát PCCC - Cứu hộ cứu nạn và công an địa phương có mặt ngay sau đó. Các nạn nhân được đưa vào bệnh viện. Hiện trường được phong toả chặt. "Sự việc khiến 2 người tử vong, một người bị thương", lãnh đạo Cảnh sát PCCC cho biết.
Nạn nhân là ông Lê Minh Tiền (59 tuổi, quê Sóc Trăng) và ông Qúy (ngoài 50 tuổi, quê Long An).
Hiện trường các nạn nhân gặp nạn. Ảnh người dân cung cấp.
"Tiếng nổ như bom làm bà con ở đây ai cũng hốt hoảng. Nhìn vào trong xưởng tôi thấy một nạn nhân không còn nguyên vẹn, trông thương lắm", nhân chứng cho hay.
Đến 10h, cảnh sát tiếp tục lấy lời khai nhân chứng, khám hiệm hiện trường, điều tra sự cố.
Sơn Hoà