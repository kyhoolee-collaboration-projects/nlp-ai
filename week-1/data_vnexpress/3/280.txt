Ngày 7/12, ông Lê Minh Hải - Trưởng phòng Kinh tế TP Cam Ranh - cho hay, có hơn 6.500 lồng tôm hùm của các hộ dân nuôi trên vịnh Cam Ranh bị chết, tổng thiệt hại khoảng 330 tỷ đồng.
"Số lượng tôm chết nhiều, kéo dài nhiều ngày nên chưa thể thống kế số lượng con mà chỉ tính tổng quát", ông Hải nói và cho biết, đến nay tình trạng tôm chết đã được cải thiện.
Người dân ở Cam Ranh vớt tôm hùm bán thương lái, mong gỡ vốn, hôm 3/12. Ảnh: Xuân Ngọc.
Về nguyên nhân, Chi cục thủy sản tỉnh Khánh Hòa cho biết, sau bão Usagi (bão số 9) ít hôm, vùng nuôi trên vịnh Cam Ranh, cách bờ biển 500 m trở vào dày đặc chất thải và bùn đất. Nước cách mặt chừng 2-3 m đục ngầu.
Các lần lấy mẫu kiểm tra cho thấy, các chỉ tiêu môi trường trong khu vực này, như: oxy hòa tan, độ mặn hay độ PH đều thay đổi, tác động đến quá trình hô hấp khiến tôm bị chết.
Ngoài ra, nhiều lồng bè nuôi tôm dày đặc cùng thức ăn dư thừa và chất thải khiến vùng nước ô nhiễm cũng là nguyên nhân. "Hiện tượng tôm hùm nuôi trong lồng bè ở vịnh Cam Ranh chết hàng loạt do ô nhiễm môi trường", Chi cục thủy sản Khánh Hòa thông tin.
Chi cục thủy sản tỉnh khuyến cáo người nuôi thủy sản cần chuyển lồng bè đến vùng đã được quy hoạch; thường xuyên kiểm tra, vệ sinh lưới, vớt rác, tạo môi trường thông thoáng cho loài nuôi phát triển.
Khoảng tuần trước, tôm hùm trọng lượng 0,3-1 kg quẫy nước mạnh, chết hàng loạt. Người dân phải vớt tôm bán tháo cho thương lái với giá rẻ mong gỡ vốn.
Người dân Khánh Hòa bán tháo do tôm hùm chết hàng loạt
Tôm chết hàng loạt, người dân vớt bán tháo, hôm 3/12.
Xuân Ngọc