Tối 16/11, hàng nghìn người dân và du khách cùng đổ về phố đi bộ Nguyễn Huệ (quận 1, TP HCM) để xem trận đấu giữa tuyển Việt Nam - Malaysia. Tại đây, 5 màn hình led cỡ lớn được lắp đặt từ sáng để phục vụ các cổ động viên.
Chứng kiến Công Phượng mở tỷ số cho tuyển Việt Nam vào phút thứ 11, người dân cùng hân hoan reo hò ăn mừng. "Việt Nam sẽ vô địch", anh Nguyễn Minh Hùng (quận Gò Vấp) hét vang.
Màn hình led tại phố đi bộ tường thuật trực tiếp cảnh Quang Hải bá vai Công Phượng mừng bàn thắng đầu tiên vào lưới Malaysia.
"Đây là lần đầu tiên tôi cổ vũ tuyển Việt Nam tại phố đi bộ này. Tôi tin đội tuyển sẽ giành chiến thắng", bà Lê Diễm, một Việt kiều đang định cư tại Singapore, chia sẻ.
Nữ du khách người Hàn Quốc mặc áo cờ đỏ sao vàng, cổ vũ cho thầy trò Park Hang-seo.
Được mẹ đeo băng rôn và miếng dán cờ Tổ quốc, bé Suri (18 tháng tuổi, quận 6) mừng rỡ theo dõi trận đấu.
Người dân và du khách hồi hộp theo dõi diễn biến trận đấu.
Khoảnh khắc các cổ động viên vỡ òa khi tuyển Việt Nam có bàn thắng ấn định tỷ số 2-0 ở phút 60 của trận đấu. Đó là pha bóng mà tiền đạo Anh Đức đã phá bẫy việt vị và dứt điểm chân trái hiểm hóc đánh bại Khairul.
Anh Nguyễn Hoàng ăn mừng bàn thắng thứ hai của tuyển Việt Nam.
Ông Phạm Văn Trường (42 tuổi, quê Phú Yên) ngồi xe lăn đi bán vé số trên phố, chăm chú theo dõi trận đấu.
Khi trọng tài người Ả-rập Xê-út thổi còi kết thúc trận đấu, người hâm mộ cùng đứng dậy, ăn mừng chiến thắng của đội nhà trước tuyển Malaysia.
Sau hai trận thắng, Việt Nam cùng được 6 điểm như Myanmar nhưng xếp sau do ghi bàn ít hơn (5-7). Nếu giành chiến thắng trước Myanmar trong chuyến làm khách ngày 20/11, thầy Park Hạng-seo sẽ giành vé vào bán kết trước một lượt đấu. Nếu hòa hoặc thua, Việt Nam vẫn còn rộng cửa vào bán kết khi chỉ tiếp Campuchia tại Mỹ Đình ở trận đấu cuối vòng bảng vào ngày 24/11.
Người hâm mộ Sài Gòn tràn ra đường ăn mừng chiến thắng của tuyển Việt Nam ở khu trung tâm.
Việt Nam 2-0 Malaysia
Diễn biến trận đấu.
Thành Nguyễn - Hữu Khoa