Khoảng gần 1h ngày 15/12, tiệm bánh kem hai tầng trên đường Phan Đăng Lưu (phường Phú Hòa, TP Huế) bất ngờ đổ sập. Lúc này có hai nhân viên tiệm bánh ngủ ở tầng 2 nhưng may mắn chỉ bị thương nhẹ.
Hiện trường vụ sập nhà. Ảnh: Võ Thạnh
Một người dân ở gần đó cho biết, "căn nhà này xây từ thời Pháp, có thể do mưa lớn nhiều ngày nước ngấm vào nên bị sập. Trước đây, chủ nhà cũng định tu sửa nhưng chưa thực hiện".
Tại hiện trường, tầng hai của ngôi nhà đổ xuống mặt đường, nhiều mảng gạch nằm vương vãi xung quanh. Nhà chức trách đang làm rõ nguyên nhân.
Ngôi nhà hai tầng ở Huế đổ sập trong đêm mưa
Video tiệm bánh hai tầng đồ sập ở Huế.
Võ Thạnh