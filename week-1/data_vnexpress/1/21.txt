Tôi sinh năm 1987, quê Hà Nam, đã vào Biên Hòa, Đồng Nai được 21 năm. Nhà có 4 anh chị em, bố mẹ vẫn mạnh khỏe. Tôi làm chuyên về ngành dịch vụ kỹ thuật sửa chữa, bảo trì, thiết kế máy móc và thương mại thiết bị điện công nghiệp.
Mấy năm trước, tôi làm trưởng phòng kỹ thuật cho mấy công ty. Do không thích bị gò bó về thời gian nên gần đây đã nghỉ làm và mở công ty riêng như đã trình bày ở trên.
Thu nhập ổn định nên tôi rất muốn tìm bạn gái để làm quen, tìm hiểu và tiến đến hôn nhân nếu thực sự hợp và hiểu nhau.
Tôi mong bạn gái bằng hoặc hơn vài tuổi, chỉ cần cao khoảng 1,6 m, có nghề nghiệp ổn định, trắng trẻo, xinh xắn một chút, sống ở TP HCM hoặc Biên Hòa.
Xem thêm chia sẻ khác tại đây
Họ tên: Đinh Quang Thể
Tuổi: 31 tuổi
Nghề nghiệp: Kỹ thuật
Nơi ở: Thành phố Biên Hòa, Đồng Nai
Giới tính: Nam
Liên lạc với tác giả