Tôi sinh ra và lớn lên tại Tân Uyên, Bình Dương. Tuổi Giáp Tý thường có số phận lận đận cả về tình duyên, công việc và cuộc sống, nhưng người ta hay nói sau vất vả sẽ gặt hái được nhiều thành công và những điều tốt đẹp. Tôi cũng hy vọng mình sẽ được như thế.
Tôi đã ly hôn, đang sống cùng con gái và con trai. Cao 1,56 m, nặng 49 kg, tôi được mọi người nhận xét là ngoại hình cân đối, gương mặt ưa nhìn, có hai má lúm đồng tiền. Còn bản thân tự thấy mình cũng ưa nhìn thôi.
Tôi sống nội tâm và luôn suy nghĩ cho người khác, nhưng mọi việc dường như đều phản bội tôi. Công việc kế toán đôi khi cũng khiến tính cách của bản thân hơi khó chịu, cẩn thận và cầu toàn. Tôi rất thích đi du lịch, ngồi trên bãi biển ngắm bình minh và hoàng hôn.
Trong cuộc sống, có nhiều chuyện xảy ra nhưng tôi luôn cố gắng vượt qua tất cả. Tôi mong tìm được một người hiểu, chia sẻ công việc, cuộc sống và quan tâm đến mình. Hy vọng gặp được người có cùng quan điểm và sở thích.
Xem thêm chia sẻ khác tại đây
Họ tên: Phạm Thị Bích Tuyền
Tuổi: 34 tuổi
Nghề nghiệp: Văn phòng
Nơi ở: Huyện Tân Uyên, Bình Dương
Giới tính: Nữ
Liên lạc với tác giả