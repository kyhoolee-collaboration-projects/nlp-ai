Tôi sinh năm 1987, con gái tuổi Đinh thường vất vả. Đúng vậy, tôi vất vả về đường tình duyên. Tôi ly hôn 2 năm, hiện sống cùng con trai 5 tuổi ở ngoại thành Hà Nội. Tôi làm trong nhà nước, thu nhập đủ để hai mẹ con sống thoải mái, du lịch và làm đẹp.
Tôi sống nội tâm, chân thành, thẳng thắn và thật thà, ít giao tiếp xã hội vì đặc thù công việc. Bề ngoài mạnh mẽ, hay cười nhưng ẩn bên trong tôi là những nỗi buồn không biết tỏ cùng ai. Tôi hay khóc nhưng rồi tự lau đi. Dù đêm hôm trước khóc sưng mắt, sáng hôm sau vẫn rạng rỡ đi làm. Quá khứ có quá nhiều nỗi đau nhưng tôi luôn nhắc mình phải đứng vững. Bây giờ có lẽ không gì khiến tôi gục ngã được. Nhưng là phụ nữ, tôi vẫn luôn mong có một bờ vai để dựa vào, chia sẻ nỗi niềm trong cuộc sống.
Tôi giỏi may vá, còn nấu ăn tuy chưa giỏi nhưng không đến nỗi quá tệ. Thích du lịch trải nghiệm, thỉnh thoảng tham gia các nhóm từ thiện, yêu thích nhạc không lời, trồng hoa, đặc biệt là hoa hồng.
Qua chuyên mục này, tôi hy vọng làm quen với những người có cùng hoàn cảnh, đã ly hôn hoặc vợ mất, công việc ổn định; không quan trọng nuôi con hay không vì quan điểm của tôi là: tôi yêu những đứa trẻ, vậy sao không yêu nổi con của chồng.
Xem thêm chia sẻ khác tại đây
Họ tên: Trần trang
Tuổi: 32 tuổi
Nghề nghiệp: Giáo viên
Nơi ở: Huyện Phúc Thọ, Hà Nội
Giới tính: Nữ
Liên lạc với tác giả