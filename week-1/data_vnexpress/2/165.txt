Tăng Thanh Hà có sở thích dùng kính mắt làm điểm nhấn thay vì giày hay túi xách, phù hợp với phong cách thanh lịch và cổ điển mà cô hướng tới. Bộ sưu tập kính hàng hiệu của cô gồm nhiều phong cách, từ kiểu mắt mèo quý phái của những năm 1950, kính tròn to ấn tượng, kính gọng vuông năng động, kính phi công, kính tròng màu của thập niên 1970... Gương mặt trái xoan giúp cô dễ dàng hợp với mọi loại kính.
Nữ diễn viên thường xuyên phối kính trong phong cách mặc thường ngày, giúp nâng tầm diện mạo thời trang. Với crop top kẻ và quần cạp cao màu vàng mang phong cách retro, cô chọn kính gọng trắng từng gây sốt ở thập niên 1970.
Người đẹp phối kính mắt mèo với áo lụa trắng, chân váy xếp bèo. Loại kính này được các ngôi sao và tín đồ thời trang thế giới ưa chuộng từ thập niên 1950 - 1960.
Hà Tăng theo đuổi mốt kính tý hon rộ lên trong những năm 1990 và là xu hướng hot của năm ngoái.
Kính tròn unisex được cô kết hợp cùng áo khoác kẻ và váy đen đơn giản.
Cùng kiểu kính này, cô biến tấu với áo phông trắng, jumpsuit kẻ sọc.
Bộ đồ đơn giản mà vẫn hợp mốt với dép lê, túi xách cao su và váy dây rút - xu hướng hot của năm nay.
Cô kết hợp áo phông năng động với kính tròn cổ điển pha chút hippie.
Bà mẹ hai con trung thành với kiểu mặc tối giản và đơn sắc nên rất dễ phối cùng các loại kính.
Bộ suit họa tiết chim hạc mang phong cách thanh lịch ăn ý cùng kính mắt mèo.
Tăng Thanh Hà theo đuổi kính mắt tròng màu được làng mốt thế giới lăng xê từ những năm 1970.
Công thức phối cả bộ denim với áo kẻ ngang, kính mắt vừa đơn giản, vừa hiệu quả.