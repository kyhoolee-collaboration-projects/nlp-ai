Catriona Gray đăng quang Hoa hậu Hoàn vũ trong chung kết sáng 17/12 tại Bangkok, Thái Lan. Đây là kết quả không gây bất ngờ bởi ngay từ khi cuộc thi bắt đầu, Catriona Gray đã là ứng viên số một cho ngôi hoa hậu.
Người đẹp Philippines đăng quang Hoa hậu Hoàn vũ 2018
Giây phút đăng quang của Catriona Gray.
Catriona Gray nhận vương miện Mikimoto - với biểu tượng cánh chim phượng hoàng tung bay - từ người tiền nhiệm Demi-Leigh Nel-Peters.
Đại diện Philippines không chỉ được đánh giá cao về hình thể, khả năng ngoại ngữ mà còn có kỹ năng thuyết trình. Cô luôn tự tin, đi thẳng vào vấn đề khiến người nghe đồng cảm.
Với câu hỏi ứng xử chung: "Bài học quan trọng nhất trong đời bạn là gì và bạn áp dụng điều đó như thế nào nếu giữ vương miện Miss Universe?", Catriona đưa vào hoàn cảnh cụ thể của bản thân. Cô nói: "Tôi làm việc rất nhiều trong các khu ổ chuột ở Tondo, Manila và cuộc sống ở đó vô cùng nghèo khó, đáng buồn. Và tôi luôn dạy bản thân phải tìm kiếm những điều tốt đẹp trong đó, tìm kiếm vẻ đẹp trên những gương mặt trẻ thơ và biết ơn. Và tôi sẽ mang theo điều này khi là Hoa hậu Hoàn vũ, để nhìn nhận các vấn đề với sự lạc quan, để đánh giá nơi nào tôi có thể chia sẻ, nơi nào tôi có thể hỗ trợ với tư cách một người phát ngôn. Nếu tôi có thể dạy mọi người biết ơn, chúng ta có thể tạo nên một thế giới nơi sự tiêu cực không thể phát triển, trẻ em sẽ có nụ cười trên gương mặt".
Ở đêm bán kết, Catriona Gray cũng khiến khán giả xôn xao vì bài thi xuất sắc và điệu catwalk “nham thạch” (Lava Walk) - mô phỏng chuyển động của dòng nham thạch, gồm trượt, đung đưa và xoay mạnh. Siêu mẫu Tyra Banks thán phục trước phần thi và xem Catriona Gray là đỉnh cao của nhan sắc Philippines.
Mỹ nhân Philippines ở đêm thi bán kết
Điệu catwalk của Catriona Gray ở bán kết.
Phần thi đêm bán kết giúp Catriona Gray có được chiếc cúp "Missosologist's Choice" (Hoa hậu được yêu thích nhất). Chuyên trang sắc đẹp Missosology cũng đoán cô đăng quang.
Trước khi có được thành công tại Miss Universe 2018, Catriona Gray từng có hành trình thất bại ở Miss World 2016.
Catriona Gray đăng quang Miss World Philippines 2016
Catriona Gray khi trở thành Hoa hậu Thế giới Philippines 2016.
Tại Miss World 2016, Catriona Gray cũng là ứng viên số một cho ngôi hoa hậu. Ở các phần thi phụ, Catriona Gray thắng "Hoa hậu truyền thông", về thứ hai "Hoa hậu tài năng" và vào Top 5 "Hoa hậu nhân ái". Nhiều chuyên trang nhan sắc dự đoán cô đăng quang. Tuy nhiên, cô mất ngôi hoa hậu về tay người đẹp Stephanie Del Valle của Puerto Rico.
Catriona Gray khóc tại sân khấu Miss World 2016
Trong đêm chung kết Miss World 2016 ở Mỹ, người hâm mộ chứng kiến Catriona Gray ôm lá cờ tổ quốc đầy xúc động.
Catriona Gray im ắng suốt nửa năm và nói muốn tập trung vào âm nhạc cùng công việc người mẫu. Nhưng sau đó cô đăng ký thi Hoa hậu Hoàn vũ Philippines 2017 trước sự động viên của người hâm mộ. Cô đăng quang và một lần nữa đại diện đất nước dự thi Miss Universe.
Để phù hợp tiêu chí Miss Universe, Catriona Gray thay đổi hình ảnh từ dịu dàng sang quyến rũ. Cô luyện tập để có hình thể nóng bỏng. Người đẹp cũng tập catwalk, học giao tiếp...
Suốt một tháng ở Thái Lan dự thi Miss Universe, khán giả không còn nhận ra cô gái từng nức nở ôm quốc kỳ khóc trên sân khấu Miss World. Ở Catriona Gray luôn toát ra sự tự tin, thu hút mọi ánh nhìn khi xuất hiện.
Ở họp báo sau chung kết, Catriona Gray nói cô không thể tin mình đã giành chiến thắng và gửi lời cảm ơn khán giả Philippines đã luôn ủng hộ cô. Điều đầu tiên cô muốn làm sau đăng quang là gặp gia đình. Bạn trai có mặt ở Thái Lan để cổ vũ tinh thần người đẹp.