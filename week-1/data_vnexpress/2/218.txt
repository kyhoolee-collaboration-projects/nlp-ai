Tối 15/12, trận chung kết lượt về giữa đội tuyển Việt Nam - Malaysia diễn ra tại sân Mỹ Đình. Ở hiệp một, Việt Nam dẫn trước 1-0 với bàn thắng của Anh Đức. Tỷ số được đội nhà nỗ lực giữ đến kết thúc hiệp hai.
Diễn viên Phương Oanh xem bóng đá cùng thầy cô, sinh viên Đại học Thăng Long ở Hà Nội. Cô reo hò ầm ĩ khi hồi còi báo hiệu trận đấu vang lên.
Cập nhật sao Việt vui mừng
Mỹ Tâm lái xe đạp tí hon chạy xung quanh khu nhà cô đang ở tại TP HCM để thể hiện niềm vui.
Ngọc Diễm xuống phố hòa niềm vui cùng hàng triệu khán giả.
Cập nhật sao Việt vui mừng
Nhà thiết kế Lê Thanh Hòa cũng tham gia vào dòng người đổ xuống đường ăn mừng trên đường phố Sài Gòn.
Hoàng Thùy Linh đi xe mui trần hòa vào dòng người đông đúc tại trung tâm TP HCM.
100 sao Việt hát 'Khát khao chiến thắng' trên phố đi bộ Nguyễn Huệ
100 nghệ sĩ hát "Khát khao chiến thắng" tại phố đi bộ Nguyễn Huệ (TP HCM) vào khoảng 18h chiều 15/12 để cổ vũ tinh thần đội nhà.
Đang công tác ở Hội An, Nguyên Khang đạp xe cùng người dân ở đây, vẫy cờ chúc mừng đội tuyển.
Lý Hùng vẫy cờ Hàn Quốc sau chiến thắng của đội nhà trên sân Mỹ Đình. Anh nhận xét đội bạn tăng sức ép lên đội tuyển áo đỏ. "Tôi ấn tượng với pha đấm bóng giữ vững khung thành của Văn Lâm", Lý Hùng chia sẻ. Trước đó, anh và nhiều nghệ sĩ từ TP HCM bay ra Hà Nội để xem trận đấu trực tiếp.
Lý Hùng và hàng chục nghìn CĐV ở Mỹ Đình ăn mừng Việt Nam vô địch
Lý Hùng cùng bạn bè reo hò trên khán đài sân Mỹ Đình.
Cập nhật không khí ăn mừng
Ca sĩ Vy Oanh hát vang ca khúc "Việt Nam ơi" mừng đội nhà chiến thắng Malaysia để vô địch AFF Cup.
Vợ ca sĩ Đăng Khôi ra phố Nguyễn Huệ (TP HCM) từ sớm để xem bóng đá cùng hơn 10.000 cổ động viên.
Từ trái qua: Hoa hậu Ngọc Diễm, Tiểu Vy, Mỹ Linh, Ngọc Hân và Á hậu Huyền My cùng reo mừng.
Cập nhật sao Việt vui mừng
Phương Thanh giữa hàng chục nghìn cổ động viên ở sân Mỹ Đình.
Ca sĩ Phương Thanh hét khản cổ cùng cổ động viên khi đội nhà ghi bàn thắng.
"Theo tôi trong trận chung kết lượt về. Malaysia thi đấu kém hơn đội tuyển Việt Nam so với trận lượt đi. Họ bị sức ép dưới sự cổ vũ nhiệt tình của hơn 40.000 cổ động viên Việt Nam. Xin chúc mừng đội tuyển Việt Nam và người hùng trong trận đấu - Anh Đức. Cảm ơn thầy Park - người đã thổi một luồng gió mới vào đời sống bóng đá Việt suốt một năm qua", người mẫu Hồ Đức Vĩnh chia sẻ.
đạo diễn Chánh Trực xúc động khi Việt Nam vô địch
Đạo diễn Chánh Trực xúc động trước chiến thắng.
* Hàng trăm sao Việt hát, ra sân cổ vũ đội nhà trước chung kết AFF Cup
Vợ chồng Á hậu Trịnh Kim Chi bế con gái xuống phố xem bóng đá.
* Minh Luân xuống đường khi trận đấu kết thúc
Minh Luân đi bão sau trận đấu
>> Xem thêm: Diễn viên, ca sĩ Việt hòa niềm vui Việt Nam vô địch AFF Cup
Dung Nhật An