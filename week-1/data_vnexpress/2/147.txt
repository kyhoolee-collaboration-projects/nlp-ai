A Simple Favor (Lời thỉnh cầu bí ẩn) mang hơi hướng ly kỳ, trinh thám giống tác phẩm Gone Girl từng gây sốt năm 2014. Phim đề cập tới những góc khuất trong cuộc hôn nhân tưởng chừng đẹp đẽ của Emily và Sean.
Trailer 'A Simple Favor'
Trailer phim "A Simple Favor".
Bối cảnh phim diễn ra tại một thị trấn nhỏ. Stephanie Smothers (Anna Kendrick đóng), một vlogger làm nghề nội trợ, cố gắng tìm hiểu sự thật liên quan tới sự biến mất của cô bạn thân Emily Nelson (Blake Lively thủ vai). Cùng chồng của Emily là Sean (Henry Golding), Stephanie dấn thân vào hành trình kịch tính, đầy nút thắt bất ngờ.
Blake Lively (phải) và Anna Kendrick, hai nữ chính của phim.
Henry Golding là tay ngang trong làng điện ảnh nhưng có thành công vang dội năm nay. Bộ phim Crazy Rich Asians của anh được khán giả Mỹ đón nhận và đưa Henry trở thành ngôi sao mới ở Hollywood. Trước đây anh chỉ là người dẫn chương trình du lịch của kênh Discovery. Nhờ Crazy Rich Asians, Henry Golding được đạo diễn Paul Feig mời đóng A Simple Favor. Dù nội dung chính của phim tập trung vào màn đối đầu giữa Emily và Stephanie, vai trò người chồng Sean của Henry Golding cũng là yếu tố thú vị, tạo kịch tính. Sau A Simple Favor, Henry Golding tiếp tục hợp tác với đạo diễn Paul Feig trong tác phẩm Last Christmas đóng cùng "Mẹ Rồng" Emilia Clarke.
Henry Golding khi dẫn chương trình du lịch
Henry Golding khi dẫn chương trình du lịch.
Henry Golding mô tả về vai diễn: "Sean từng là cây bút nổi tiếng tại New York và điều đó khiến anh lọt vào tầm ngắm của Emily. Cô quyến rũ, quyền lực và cực kỳ mạnh mẽ. Emily là tất cả với Sean, nhất là từ khi anh chấm dứt nghiệp viết lách. Khi Emily mất tích, Sean suy sụp và tìm tới Stephanie".
Tác phẩm của đạo diễn Paul Feig được dựng dựa trên cuốn tiểu thuyết cùng tên của Darcey Bell. Trước đây, Paul Feig nổi tiếng với những tác phẩm hài như Bridemaids (2011), Spy (2015), Ghostbusters (2016)...
A Simple Favor ra rạp tại Việt Nam từ ngày 19/10.
Thùy Liên