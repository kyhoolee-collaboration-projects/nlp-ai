Cùng nhau viết chuyện tương lai do nhạc sĩ trẻ Huỳnh Hiền Năng sáng tác, gói ghém thông điệp truyền cảm hứng, cổ vũ những cô gái hiện đại biết yêu thương bản thân. Lân Nhã, Karik và Quang Bảo cùng hát và ghi hình cho MV ca khúc.
Từ trái sang: Quang Bảo, Karik và Lân Nhã.
MV có kinh phí thấp nhưng phần hình ảnh được thực hiện chỉn chu với tông màu xanh, trắng tươi sáng, hiện đại. Lời bài hát là những câu nói ngọt ngào của chàng trai dành cho một nửa: "Từ hôm nay xin em hãy yêu thương bản thân nhiều hơn"; "Còn chờ gì mà không tính đi em"...
Nhạc sĩ Huỳnh Hiền Năng chia sẻ khi sáng tác bài hát, anh đã nghĩ về những người phụ nữ thân thương bên cạnh. Mẹ, chị gái, em gái, bạn gái anh... là những người luôn vun vén, chăm sóc cho người khác nhưng thường lơ là bản thân. "Muốn bay thật cao, đi thật xa thì hành trang cần thiết nhất là sức khỏe”, anh gởi gắm.
MV "Cùng nhau viết chuyện tương lai"
Lân Nhã, Karik ra MV kêu gọi phụ nữ yêu thương bản thân hơn
Rapper Karik - chủ nhân bản hit Người lạ ơi - chấp bút cho phần rap của bài hát này. Lân Nhã là top 4 Việt Nam Idol 2010, còn Quang Bảo là gương mặt MC quen thuộc với khán giả.
Ca khúc được Hội Y học Dự phòng Việt Nam đặt hàng cho chiến dịch "Bảo vệ sức khỏe - Làm chủ tương lai" - chương trình cộng đồng góp phần nâng cao ý thức của các cô gái trẻ trước bệnh ung thư cổ tử cung.
Vân An