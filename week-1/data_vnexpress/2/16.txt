Vương miện Hoa hậu Siêu quốc gia 2018 thuộc về người đẹp Puerto Rico. Đại diện Việt Nam - Minh Tú - vào đến Top 10 và nhận giải "Hoa hậu được yêu thích nhất" do chuyên trang sắc đẹp Missosology bình chọn.
Các màn trình diễn của Minh Tú ở Miss Supranational 2018
Minh Tú tỏa sáng trên sân khấu chung kết Miss Supranational 2018.
Hải Dương - giám đốc quốc gia cuộc thi Miss Supranational - có mặt ở Ba Lan cổ vũ tinh thần Minh Tú.
Tại cuộc thi, Minh Tú gặp áp lực bởi tin đồn mua giải. Người đẹp không giải thích mà lặng lẽ chứng minh nỗ lực của bản thân.
Hải Dương bày tỏ cô hài lòng với sự thể hiện của Minh Tú ở Miss Supranational 2018.
Minh Tú khóc khi nhận giải 'Hoa hậu được yêu thích nhất' của Missosology
Minh Tú khóc khi nhận giải 'Hoa hậu được yêu thích nhất' của Missosology.
Minh Tú chụp ảnh kỷ niệm với Hoa hậu Siêu Quốc gia 2018 - Ngọc Châu. Ngọc Châu sẽ đại diện Việt Nam tham dự cuộc thi vào năm sau.
Diễn viên Võ Cảnh (phải).
Nhà thiết kế Chung Thanh Phong là người chuẩn bị toàn bộ trang phục cho Minh Tú tại cuộc thi.
Người đẹp Minh Phương (trái) cũng sang cổ vũ Minh Tú.
Nhà thiết kế Chung Thanh Phong, Hoa hậu Hải Dương, siêu mẫu Minh Tú, MC Phan Anh, Hoa hậu Ngọc Châu (từ trái sang).
Người đẹp Venezuala, Slovakia, Minh Tú, người đẹp Philippines, Malaysia (từ trái sang).
Tại cuộc thi, Minh Tú luôn giữ hình ảnh thân thiện. Cô từng cho Hoa hậu Philippines và Guadeloupe mượn váy.
Hà Thu