Ngọc Diễm mặc áo dài đôi cùng con gái bảy tuổi trong đêm tiệc. Em trai Ngọc Diễm (trái) - tên Khánh, hiện là sinh viên - đến mừng ngày vui của chị.
Cô bé bị mẹ trêu vì vừa ngủ gục trong hậu trường do lịch học bận rộn cả ngày. Ngọc Diễm khóc gọi bé Chiko là món quà quý giá nhất trong đời. Cô làm mẹ đơn thân từ tuổi 24. Bé Chiko mong mẹ mau lấy chồng để bé có em bầu bạn.
Cuộc sống làm mẹ đơn thân của Hoa hậu Ngọc Diễm
Cuộc sống làm mẹ đơn thân của Ngọc Diễm.
Người đẹp trình diễn ca khúc "Hello Vietnam".
Ngọc Diễm từng là MC trong đêm H'Hen Niê đăng quang Hoa hậu Hoàn vũ Việt Nam 2017. Sau đó, cả hai đồng hành trong nhiều chuyến thiện nguyện.
Diễn viên "Hậu duệ mặt trời" Cao Thái Hà là bạn cùng phòng với Ngọc Diễm khi thi hoa hậu 10 năm trước.
Ngọc Diễm và Thùy Dung - Á hậu Việt Nam 2016 - cùng trưởng thành từ đại học Ngoại thương TP HCM.
Phan Thị Mơ dự sự kiện sau khi đăng quang Hoa hậu Đại sứ Du lịch Thế giới ở Thái Lan.
Nhà thiết kế Sĩ Hoàng bên người đẹp Loan Vương. Cô đoạt giải "Gương mặt ấn tượng nhất" trong cùng cuộc thi với Ngọc Diễm, hiện là tiếp viên hãng hàng không quốc gia.
Tam Kỳ