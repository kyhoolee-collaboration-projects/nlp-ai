Hồ Ngọc Hà vừa có đêm nhạc ra mắt ca khúc ballad mới - Giá như mình đã bao dung – sáng tác của nhạc sĩ Nguyễn Hồng Thuận viết riêng cho cô. Kim Lý (thứ ba từ phải sang) cũng xuất hiện cùng gia đình bạn gái.
Không gian phòng trà khá chật hẹp so với lượng người có mặt. Diễn viên nhường ghế cho khán giả, còn mình đứng ở góc xa nhất để theo dõi các tiết mục trình diễn của Hồ Ngọc Hà. Từ khi yêu nhau, Kim Lý thường xuyên đưa Hồ Ngọc Hà đi diễn, đi sự kiện. Diễn viên cũng góp mặt trong hai MV của người yêu là "Cả một trời thương nhớ" và "Em muốn anh đưa em về".
Cả hai quen nhau qua một người bạn. Sau khi đóng cặp trong MV "Cả một trời thương nhớ" vào năm 2017, họ thường đi chung trong các sự kiện nhưng chưa bao giờ khẳng định yêu nhau. Tối 14/4, trong một sự kiện ở Hà Nội, Kim Lý lần đầu chia sẻ về chuyện tâm sự cô muốn có con với Kim Lý nhưng chưa nghĩ đến chuyện cưới hỏi.
Hà Hồ lần đầu thể hiện ca khúc mới
Hà Hồ hát Giá như mình bao dung.
Cô ngẫu hứng cover Hongkong 1, ca khúc đang gây sốt trên mạng xã hội, theo yêu cầu từ khán giả.
Diện đầm voan đỏ phong cách kiêu sa, Hồ Ngọc Hà liên tục trình diễn gần 20 ca khúc ballad quen thuộc được khán giả yêu thích như Gửi người yêu cũ, Cả một trời thương nhớ, Tội lỗi, Xóa ký ức, Anh còn nợ em, Sa mạc tình yêu… Ca khúc chính chủ đề của đêm nhạc Giá như mình đã bao dung cũng tạo được hiệu ứng tốt từ khán giả khi trình diễn lần đầu ở đây.
Không chỉ biểu diễn, Hồ Ngọc Hà còn dẫn dắt chương trình bằng những câu chuyện hài hước, dí dỏm khiến khán giả liên tục vỗ tay tán thưởng.
Hà Hồ chia sẻ cô vui vì khán giả đã có mặt và lấp kín sân khấu từ sớm. Cô cho biết mình luôn có cảm xúc hơn cả khi biểu diễn ở phòng trà vì không gian lắng động, gần gũi với khán giả.
Sau đêm nhạc, ca sĩ nán lại chụp ảnh cùng người hâm mộ. Cô sẽ có thêm các buổi giới thiệu ca khúc mới vào ngày 4/11 tại Hà Nội và 9/11 tại Đà Nẵng.
Hà Hồ và bố mẹ.