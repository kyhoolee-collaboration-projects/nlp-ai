Lễ ăn hỏi của Á hậu Thanh Tú diễn ra ngày 22/11 tại nhà của cô ở Hà Nội. Trong ảnh: Thanh Tú chụp ảnh kỷ niệm trước khi tạm biệt những chú gấu bông yêu thích để về nhà chồng.
Mỹ Linh, một trong những người bạn thân thiết nhất của Thanh Tú, xúc động khi tiễn cô lên xe hoa. "Chồng chị Tú rất yêu thương và chiều chuộng chị ấy. Tôi mừng cho chị đã tìm được nơi xứng đáng để nương tựa".
Chú rể xin phép mẹ Thanh Tú dắt vợ ra chào hai họ.
Chồng cô là chủ tịch một tập đoàn lớn ở Hà Nội.
Người đẹp giúp chồng chỉnh sửa trang phục trước giờ làm lễ.
Cô dâu, chú rể rót trà mời hai họ. Không gian lễ ăn hỏi có tông màu trắng, được bài trí theo phong cách sang trọng, tinh khiết, lấy ý tưởng từ mối tình đầu trong sáng của cô dâu.
Á hậu và chồng được khen xứng đôi khi đứng cạnh nhau. Cô nói mình may mắn khi tìm được người đàn ông tâm lý, nhường nhịn và tôn trọng vợ.
Thanh Tú nói đùa vì cô quá cao (1,81 m) nên khó khăn lắm mới tìm được một người đàn ông cao hơn mình để nép vào.
Á hậu xúc động khi tạm biệt bố mẹ. Tiệc cưới của cô sẽ diễn ra tối 2/12 tại Hà Nội.
Á hậu Thanh Tú cưới chồng doanh nhân đầu tháng 12
Á hậu Thanh Tú dạo chơi Nhật Bản
Ảnh: Lê Chí Linh