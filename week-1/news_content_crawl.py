import urllib
from urllib.request import urlopen
from bs4 import BeautifulSoup
import time
import sys

def parse_news_url(cate_url, idx):
	result = set()
	url = cate_url + idx 

	page = urlopen(url).read()

	soup = BeautifulSoup(page, "lxml")
	soup.prettify()
	for anchor in soup.findAll('a', href=True):
		news_url = clean_news_url(anchor['href'])
		if news_url.startswith(cate_url):
			result.add(news_url)

	return result


def clean_news_url(news_url):
	index = news_url.find('.html') + len('.html')
	news_url = news_url[0:index]
	return news_url


def crawl_category(base_cate_url):
	result = set()
	for i in range(1,10):
		time.sleep(1)
		cate_url = base_cate_url
		idx = '-p' + str(i)

		news_url_set = parse_news_url(cate_url, idx)
		result.update(news_url_set)

	for url in result:
		print(url)


def crawl_news_url(news_url):
	page = urlopen(news_url).read()

	soup = BeautifulSoup(page, "lxml")
	soup.prettify()
	
	content = soup.find("article", {"class": "content_detail"})
	data = content.get_text().strip('\n')
	lines = [ x.replace('\n', '').strip() for x in data.split('\n')]
	lines = [ x for x in lines if len(x) > 0]
	data = ('\n').join(lines)
	#print(data)
	return data


def crawl_cate_news(cate_file, cate_idx):
	with open(cate_file) as f:
		content = f.readlines()
	# you may also want to remove whitespace characters like `\n` at the end of each line
	content = [x.strip() for x in content] 
	for i in range(0, len(content)):
		try:
			news_url = content[i]
			news_data = crawl_news_url(news_url)
			path = 'data_vnexpress/' + str(cate_idx) + '/' + str(i) + '.txt'
			file = open(path, 'w') 
			file.write(news_data) 
			file.close() 
		except (urllib.error.HTTPError, AttributeError) as e:
			print("Oops!",sys.exc_info()[0],"occured.")
			print("Next entry.")
			print()


if __name__ == '__main__':
	# cate_url = 'https://vnexpress.net/thoi-su'
	# crawl_category(cate_url)
	# news_url = 'https://vnexpress.net/giai-tri/da-n-nguo-i-de-p-miss-universe-tha-da-ng-bikini-tren-bie-n-tha-i-lan-3850729.html'
	# crawl_news_url(news_url)
	crawl_cate_news('3_thoi_su.txt', 3)